# Frontend Mentor - 3-column preview card component solution

This is a solution to the [3-column preview card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/3column-preview-card-component-pH92eAR2-). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Frontend Mentor - 3-column preview card component solution](#frontend-mentor---3-column-preview-card-component-solution)
  - [Table of contents](#table-of-contents)
  - [Overview](#overview)
    - [The challenge](#the-challenge)
    - [Screenshot](#screenshot)
      - [Mobile Layout](#mobile-layout)
      - [Desktop Layout](#desktop-layout)
    - [Links](#links)
  - [My process](#my-process)
    - [Built with](#built-with)
  - [Author](#author)

## Overview

### The challenge

Users should be able to:

- View the optimal layout depending on their device's screen size
- See hover states for interactive elements

### Screenshot
#### Mobile Layout
![Mobile Layout](./assets/solution-images/solution-mobile.jpeg)
#### Desktop Layout
![Desktop Layout](./assets/solution-images/solution-desktop.jpeg)

### Links

- Solution URL: [Gitlab Repository](https://gitlab.com/deejayjay/frontend-mentor-3-column-preview-card)
- Live Site URL: [View my solution live](https://deejayjay-fem-3-column-layout.netlify.app)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow
- Responsive Design
- Media Queries

## Author

- Github - [DeeJayJay](github.com/deejayjay)
- Frontend Mentor - [@deejayjay](https://www.frontendmentor.io/profile/deejayjay)
- Twitter - [@deejay_the_dev](https://twitter.com/deejay_the_dev)


